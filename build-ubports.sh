#!/bin/bash

set -Eeou pipefail

# Download latest SDL
URL_LATEST=https://gitlab.com/abmyii/sdl/-/jobs/artifacts/ubports/download?job=xenial_${ARCH}_deb

wget ${URL_LATEST} -O build.zip
unzip build.zip
dpkg -i --force-overwrite build/libsdl2_*.deb
dpkg -i --force-overwrite build/libsdl2-dev_*.deb

rm /usr/lib/${ARCH_TRIPLET}/libSDL2-2.0.so.0.4.0
ls -alh /usr/lib/${ARCH_TRIPLET}/libSDL2-2.0.so*


cd "${ROOT}"
make -j$(( ( $(getconf _NPROCESSORS_ONLN) + 1) / 2 ))
